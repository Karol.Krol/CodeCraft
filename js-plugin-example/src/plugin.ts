// @ts-ignore
type Player = org.bukkit.entity.Player;
// @ts-ignore
type CommandSender = org.bukkit.command.CommandSender;
// @ts-ignore
type Command = org.bukkit.command.Command;
// @ts-ignore
type PotionEffect = org.bukkit.potion.PotionEffect;
// @ts-ignore
type PotionEffectType = org.bukkit.potion.PotionEffectType;
// @ts-ignore
type World = org.bukkit.World;
// @ts-ignore
const BlockFace = org.bukkit.block.BlockFace;
// @ts-ignore
const Material = org.bukkit.Material;
// @ts-ignore
const GameMode = org.bukkit.GameMode;
// @ts-ignore
const Bukkit = org.bukkit.Bukkit;
// @ts-ignore
const Vector = org.bukkit.util.Vector;
// @ts-ignore
type Vector = org.bukkit.util.Vector;
// @ts-ignore
const Double = Java.type("java.lang.Double");
// @ts-ignore
const Float = Java.type("java.lang.Float");
// @ts-ignore
const ProjectileHitEvent = org.bukkit.event.entity.ProjectileHitEvent;
// @ts-ignore
type ProjectileHitEvent = org.bukkit.event.entity.ProjectileHitEvent;
// @ts-ignore
const PlayerLoginEvent = org.bukkit.event.player.PlayerLoginEvent;
// @ts-ignore
type PlayerLoginEvent = org.bukkit.event.player.PlayerLoginEvent;
// @ts-ignore
const PlayerInteractAtEntityEvent = org.bukkit.event.player.PlayerInteractAtEntityEvent;
// @ts-ignore
type PlayerInteractAtEntityEvent = org.bukkit.event.player.PlayerInteractAtEntityEvent;
// @ts-ignore
const PlayerInteractEvent = org.bukkit.event.player.PlayerInteractEvent;
// @ts-ignore
type PlayerInteractEvent = org.bukkit.event.player.PlayerInteractEvent;
// @ts-ignore
const LightningStrikeEvent = org.bukkit.event.weather.LightningStrikeEvent;
// @ts-ignore
type LightningStrikeEvent = org.bukkit.event.weather.LightningStrikeEvent;
// @ts-ignore
const Action = org.bukkit.event.block.Action;
// @ts-ignore
type Action = org.bukkit.event.block.Action;
// @ts-ignore
const Entity = org.bukkit.entity.Entity;
// @ts-ignore
type Entity = org.bukkit.entity.Entity;
// @ts-ignore
const Arrow = org.bukkit.entity.Arrow;
// @ts-ignore
type Arrow = org.bukkit.entity.Arrow;
// @ts-ignore
const Player = org.bukkit.entity.Player;
// @ts-ignore
type Player = org.bukkit.entity.Player;
// @ts-ignore
const ProjectileSource = org.bukkit.projectiles.ProjectileSource;
// @ts-ignore
type ProjectileSource = org.bukkit.projectiles.ProjectileSource;

function onPlayerLoginEvent(event: PlayerLoginEvent) {
  console.log(
    `# Player: ${event
      .getPlayer()
      .getName()} # with address ${event.getAddress()} has login to server`
  );
}

function onPlayerInteractAtEntityEvent(event: PlayerInteractAtEntityEvent) {}

function onPlayerInteractEvent(event: PlayerInteractEvent) {
  // if (event.getAction() == Action.RIGHT_CLICK_AIR) {
  //     event.getPlayer().sendMessage("RIGHT_CLICK_AIR");
  // }
  // console.log(event.getAction())
}

function onLightningStrikeEvent(event: LightningStrikeEvent) {}

function onProjectileHitEvent(event: ProjectileHitEvent) {
  const entity: Entity = event.getEntity();
  if (entity instanceof Arrow) {
    const arrow = entity as Arrow;
    const shooter: ProjectileSource = arrow.getShooter();
    if (shooter instanceof Player) {
      (shooter as Player)
        .getWorld()
        .createExplosion(arrow.getLocation(), new Float(5.0));
      arrow.remove();
    }
  }
}

enum Colors {
  Black = "\033[30m",
  Red = "\x1b[31m",
  Green = "\x1b[32m",
  Yellow = "\x1b[33m",
  Blue = "\033[34m",
  Magenta = "\033[35m",
  Cyan = "\033[36m",
  White = "\033[37m",
}

function color(text: string, color: Colors) {
  return `${color}${text}\x1b[0m`;
}

function logCommandCall(
  sender: CommandSender,
  cmd: Command,
  label: string,
  args: string[]
) {
  console.log(`[${color(`CodeCraft`, Colors.Green)}] (JS) -> ${cmd.getName()}(
    ${color(`sender`, Colors.Yellow)}={ ${sender.getName()} },
    ${color(`cmd`, Colors.Yellow)}={ ${cmd} },
    ${color(`label`, Colors.Yellow)}={ ${label} },
    ${color(`args`, Colors.Yellow)}={ ${args} }
  )`);
}

function getPlayer(sender: CommandSender, args: string[]): Player {
  let player: Player = null;
  if (args.length > 0) {
    player = Bukkit.getServer().getPlayer(args[0]);
  }
  if (!player) {
    player = Bukkit.getServer().getPlayer(sender.getName());
  }
  return player;
}

function onToggleFlyCommand(
  sender: CommandSender,
  cmd: Command,
  label: string,
  args: string[]
): boolean {
  logCommandCall(sender, cmd, label, args);
  const player = getPlayer(sender, args);

  player.setAllowFlight(!player.getAllowFlight());
  if (player.getAllowFlight()) {
    player.sendMessage("Mozesz latac!");
    return true;
  } else {
    player.sendMessage("Nie mozesz juz latac.");
    return true;
  }
}

function onSetTimeCommand(
  sender: CommandSender,
  cmd: Command,
  label: string,
  args: string[]
): boolean {
  logCommandCall(sender, cmd, label, args);
  const player = getPlayer(sender, args);
  const hour: number = parseInt(args[0], 10);
  console.log(`hour=${hour}`);
  const worlds: World[] = Bukkit.getServer().getWorlds();
  worlds.forEach((world) => {
    world.setTime(hour);
  });
  player.sendMessage(`Godzina = ${hour}`);
  return true;
}

function onBuildCommand(
  sender: CommandSender,
  cmd: Command,
  label: string,
  args: string[]
) {
  logCommandCall(sender, cmd, label, args);
  const player = getPlayer(sender, args);

  const materialType = Material.DIRT;
  //   const targetBlock = player.getLocation().getBlock().getRelative(BlockFace.DOWN);
  const length: number = parseInt(args[0], 10);
  const height: number = parseInt(args[1], 10);
  const vector = getLeadFrontDirection(player);
  let location = player.getLocation();
  for (let x: number = 0; x < length; x++) {
    location = location.add(vector);
    location.getBlock().setType(materialType);
    for (let z: number = 1; z < height; z++) {
      location.add(new Vector(0, 1, 0));
      location.getBlock().setType(materialType);
    }
    location.add(new Vector(0, -(height - 1), 0));
  }
  // player.sendMessage(`Place ${materialType} in location: ${targetBlock.getLocation()}`);
  return true;
}

// function onCommand(
//   sender: CommandSender,
//   cmd: Command,
//   label: string,
//   args: string[]
// ) {

//   const player = getPlayer(sender, args);
//   if (!player) return false;
//   switch (cmd.getName()) {
//     case "toggle-fly":
//       toggleFly(player);
//       return true;
//     case "heal":
//       player.setHealth(player.getHealthScale());
//       return true;
//     case "starve":
//       player.setFoodLevel(1);
//       return true;
//     case "full":
//       player.setFoodLevel(20);
//       return true;
//     case "thunder": {
//       const worlds: World[] = Bukkit.getServer().getWorlds();
//       worlds.forEach((world) => {
//         world.setThundering(true);
//         world.setStorm(true);
//       });
//       player.sendMessage(`Burza!`);
//       return true;
//     }
//     case "thunder-stop": {
//       const worlds: World[] = Bukkit.getServer().getWorlds();
//       worlds.forEach((world) => {
//         world.setThundering(false);
//         world.setStorm(false);
//       });
//       player.sendMessage(`Burza stop!`);
//       return true;
//     }
//     case "szybciej": {
//       const newSpeed = parseFloat(player.getFlySpeed().toPrecision(4)) + 0.1;
//       if (newSpeed > 1.0) {
//         player.sendMessage(`FlySpeed = ${player.getFlySpeed()}`);
//         player.sendMessage("Osiagnales maksymalna dopuszalna predkosc latania");
//         return false;
//       }
//       player.setFlySpeed(new Float(newSpeed));
//       player.sendMessage(`FlySpeed = ${newSpeed}`);
//       return true;
//     }
//     case "wolniej": {
//       const newSpeed = parseFloat(player.getFlySpeed().toPrecision(4)) - 0.1;
//       if (newSpeed < 0.0) {
//         player.sendMessage(`FlySpeed = ${player.getFlySpeed()}`);
//         player.sendMessage("Osiagnales minimalna dopuszalna predkosc latania");
//         return false;
//       }
//       player.setFlySpeed(new Float(newSpeed));
//       player.sendMessage(`FlySpeed = ${newSpeed}`);
//       return true;
//     }
//     case "set-creative-mode": {
//       player.setGameMode(GameMode.CREATIVE);
//       player.sendMessage(`Tryb kreatywny wlączony`);
//       return true;
//     }
//     case "set-survival-mode": {
//       player.setGameMode(GameMode.SURVIVAL);
//       player.sendMessage(`Tryb przygody wlączony`);
//       return true;
//     }
// }

const getLeadFrontDirection = (player: Player): Vector => {
  const direction: Vector = player.getLocation().getDirection();
  const x = direction.getX();
  const y = direction.getY();
  const z = direction.getZ();
  if (Math.abs(x) > Math.abs(y) && Math.abs(x) > Math.abs(z))
    return new Vector(x, 0, 0).normalize();
  if (Math.abs(y) > Math.abs(x) && Math.abs(y) > Math.abs(z))
    return new Vector(0, y, 0).normalize();
  return new Vector(0, 0, z).normalize();
};

const getRightHeadDirection = (player: Player): Vector => {
  const direction: Vector = player.getLocation().getDirection().normalize();
  return new Vector(direction.getZ(), 0.0, direction.getX()).normalize();
};

const getLeftHeadDirection = (player: Player): Vector => {
  const direction: Vector = player.getLocation().getDirection().normalize();
  return new Vector(direction.getZ(), 0.0, -direction.getX()).normalize();
};

const getFrontDirection = (player: Player): Vector => {
  const direction: Vector = player.getLocation().getDirection().normalize();
  return direction.multiply(1);
};

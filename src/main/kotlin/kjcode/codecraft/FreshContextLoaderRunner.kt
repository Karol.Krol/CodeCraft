package kjcode.codecraft

internal interface FreshContextLoaderRunner {

    fun <T> runInsidePureJavaClassLoaderContext(callback: () -> T): T {
        val originalClassLoader = Thread.currentThread().contextClassLoader
        Thread.currentThread().contextClassLoader = javaClass.classLoader
        val result = callback()
        Thread.currentThread().contextClassLoader = originalClassLoader
        return result
    }
}
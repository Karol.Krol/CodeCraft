package kjcode.codecraft.spring

import kjcode.codecraft.ConsolePrinter
import kjcode.codecraft.SourceProvider
import kjcode.codecraft.bukkit.BukkitJavaScriptContextBridge
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
@ComponentScan(basePackages = ["kjcode.codecraft"])
open class CodeCraftSpringBootApp

//TODO 1: wpraz z aoi komand przeniesc do pakietu "api"
//TODO 2: zbudowac api analogiczne do tego
/*
            "js" -> jsContextBridge.executeCommand(sender, args)
            "js-reload" -> reloadJsPluginFile()
            "js-list-commands" -> listAvailableCommands(sender)
            "js-list-events" -> listRegisteredEvents(sender)
 */
//TODO 3: dodac enpoint do dodwania komand i efektow (zdarzen)
@RestController
@RequestMapping("/codeCraft")
internal open class CodeCraftController(
    private val jsContextBridge: BukkitJavaScriptContextBridge
) {

    @GetMapping("/commands")
    fun getAvailableCommands(): GetCommandsResponse = GetCommandsResponse(
        listOf(
            GetCommandsResponsePart(
                commandName = "my-command-name",
                description = "my command short description"
            )
        )
    )

    @GetMapping("/reload")
    fun reloadJs() {
        reloadJsPluginFile()
    }

    // TODO: trzeba stad usunac, (metoda zostala niemal skopiowana z CodeCraftPlugin)
    private fun reloadJsPluginFile(): Boolean {
        val file = SourceProvider.loadFile("plugin.js")
        return if (jsContextBridge.refreshContext(file!!)) {
            ConsolePrinter.println("CodeCraft", "JS Context has been reloaded")
            true
        } else false
    }

}

data class GetCommandsResponse(
    val commands: List<GetCommandsResponsePart>
)

data class GetCommandsResponsePart(
    val commandName: String,
    val description: String
)
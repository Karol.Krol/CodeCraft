package kjcode.codecraft

import io.papermc.lib.PaperLib
import kjcode.codecraft.bukkit.BukkitEventListenerWrapper
import kjcode.codecraft.bukkit.BukkitJavaScriptContextBridge
import kjcode.codecraft.spring.CodeCraftSpringBootApp
import kjcode.codecraft.spring.SpringContextHolder
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.event.Event
import org.bukkit.event.EventPriority
import org.bukkit.event.HandlerList
import org.bukkit.plugin.java.JavaPlugin
import org.springframework.boot.SpringApplication


class CodeCraftPlugin : JavaPlugin(), FreshContextLoaderRunner {

    private lateinit var jsContextBridge: BukkitJavaScriptContextBridge

    override fun onEnable() {
        PaperLib.suggestPaper(this)
        val file = SourceProvider.loadFile("plugin.js")
        runInsidePureJavaClassLoaderContext {
            SpringApplication.run(CodeCraftSpringBootApp::class.java)
            jsContextBridge = SpringContextHolder.getBean(BukkitJavaScriptContextBridge::class.java)!!
            jsContextBridge.buildContext(file!!)
        }
        refreshEventListeners()
    }

    override fun onDisable() {
        jsContextBridge.closeContext()
    }

    override fun onCommand(
        sender: CommandSender,
        cmd: Command,
        label: String,
        args: Array<String>
    ) = runInsidePureJavaClassLoaderContext {
        when (cmd.name) {
            "js" -> jsContextBridge.executeCommand(sender, args)
            "js-reload" -> reloadJsPluginFile()
            "js-list-commands" -> listAvailableCommands(sender)
            "js-list-events" -> listRegisteredEvents(sender)
            else -> false
        }
    }

    // ---- TODO: ponizsze 3 metody wyniesc do commands api

    private fun reloadJsPluginFile(): Boolean {
        val file = SourceProvider.loadFile("plugin.js")
        return if (jsContextBridge.refreshContext(file!!)) {
            refreshEventListeners()
            ConsolePrinter.println(this.name, "JS Context has been reloaded")
            true
        } else false
    }

    private fun listAvailableCommands(sender: CommandSender): Boolean {
        val availableCommands = jsContextBridge.listCommands()
        ConsolePrinter.println(name, "Available Commands: $availableCommands")
        sender.sendMessage(availableCommands.toTypedArray())
        return true
    }

    private fun listRegisteredEvents(sender: CommandSender): Boolean {
        val handledEvents = jsContextBridge.listHandledEvents()
        ConsolePrinter.println(name, "Handled Events: $handledEvents")
        sender.sendMessage(handledEvents.toTypedArray())
        return true
    }

    // ----------------------------------- koniec


    // TODO: ponizsza metoda tez powinna zawedrowac do odzielnej klasy
    private fun refreshEventListeners() {
        HandlerList.unregisterAll()
        jsContextBridge.getEventListeners().forEach { (eventType, eventListenerWrapper) ->
            registerEventListener(eventType, eventListenerWrapper)
        }
    }

    private fun <T : Event> registerEventListener(
        eventType: Class<out T>,
        listenerWrapper: BukkitEventListenerWrapper<out T>
    ) {
        server.pluginManager.registerEvent(
            eventType,
            listenerWrapper,
            EventPriority.HIGH,
            { listener, event ->
                @Suppress("UNCHECKED_CAST")
                (listener as BukkitEventListenerWrapper<T>).execute(eventType.cast(event))
            },
            this,               // TODO: <-------------- tu jedyny klopot
            true
        )
    }
}


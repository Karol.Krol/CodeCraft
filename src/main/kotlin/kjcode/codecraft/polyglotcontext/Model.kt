package kjcode.codecraft.polyglotcontext

import java.io.File

internal interface PolyglotContextBridge<T, V> {

    fun buildContext(file: File): Boolean
    fun executeCommand(sender: V, args: Array<String>): Boolean
    fun getEventListeners(): List<Pair<Class<out T>, EventListenerWrapper<T>>>
    fun listCommands(): Set<String>
    fun listHandledEvents(): List<String>
    fun refreshContext(file: File): Boolean
    fun closeContext()
}

interface EventListenerWrapper<T> {
    fun execute(event: T)
}
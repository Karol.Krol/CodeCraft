package kjcode.codecraft.polyglotcontext

import org.graalvm.polyglot.Context
import org.graalvm.polyglot.PolyglotAccess
import org.graalvm.polyglot.Source
import org.graalvm.polyglot.Value
import java.io.File

internal abstract class PolyglotContextHolder(private val languageId: String) {

    private val contextBuilder: Context.Builder = Context.newBuilder(languageId)
        .allowAllAccess(true)
        .allowPolyglotAccess(PolyglotAccess.ALL)
    private val context: Context = contextBuilder.build()
    private val binding: Value = context.getBindings(languageId)

    fun eval(file: File) = try {
        file.let { Source.newBuilder(languageId, it).build() }
            ?.let {
                context.eval(it)
                true
            } ?: false
    } catch (ex: Exception) {
        throw RuntimeException("Can not load script ${file.path}", ex)
    }

    fun <T, V> groupMembersByPolicy(
        groupingPolicy: BindingMembersGroupingPolicy<V>,
        wrapperBuilder: (fnName: String, functionBinding: Value) -> T
    ) = PolyglotContextBindingInspector.findMembersUsingPolicy(binding.memberKeys, groupingPolicy)
        .map { (commandName, fnName) ->
            val functionBinding = binding.getMember(fnName)
            commandName to wrapperBuilder(fnName, functionBinding)
        }
        .toMap()

    fun close() = context.close()
}

internal object PolyglotContextBindingInspector {

    fun <T> findMembersUsingPolicy(
        source: Collection<String>,
        policy: BindingMembersGroupingPolicy<T>
    ): Map<T, String> = source.mapNotNull { createPair(it, policy.groupingPattern, policy.transformKey) }.toMap()

    private fun <T> createPair(
        source: String,
        regexPattern: Regex,
        transformKey: (String) -> T?
    ): Pair<T, String>? = if (regexPattern.containsMatchIn(source)) {
        val item = regexPattern.findAll(source).first()
        val targetName = item.groupValues[1]
        transformKey(targetName)?.let { Pair(it, item.value) }
    } else null
}

data class BindingMembersGroupingPolicy<T>(
    val groupingPattern: Regex,
    val transformKey: (String) -> T?
)
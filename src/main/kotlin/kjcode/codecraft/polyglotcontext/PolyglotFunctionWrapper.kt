package kjcode.codecraft.polyglotcontext

import org.graalvm.polyglot.Value

internal open class PolyglotFunctionWrapper<T1, T2, T3, T4>(
    private val name: String,
    private val functionBinding: Value,
    private val executeFn: (fn: Value, p1: T1?, p2: T2?, p3: T3?, p4: T4?) -> Value
) {

    fun execute(p1: T1? = null, p2: T2? = null, p3: T3? = null, p4: T4? = null): Value {
        try {
            return executeFn(functionBinding, p1, p2, p3, p4)
        } catch (exception: Exception) {
            throw RuntimeException("Can not execute function $name", exception)
        } finally {
            unregisterParameters()
        }
    }

    private fun unregisterParameters() = functionBinding.memberKeys
        .forEach { functionBinding.removeMember(it) }
}
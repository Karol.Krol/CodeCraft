package kjcode.codecraft.bukkit

import kjcode.codecraft.polyglotcontext.EventListenerWrapper
import kjcode.codecraft.polyglotcontext.PolyglotFunctionWrapper
import org.bukkit.event.Event
import org.bukkit.event.Listener
import org.graalvm.polyglot.Value

internal class BukkitEventListenerWrapper<T : Event>
private constructor(val fnWrapper: PolyglotFunctionWrapper<T, Unit, Unit, Unit>) : Listener, EventListenerWrapper<T> {

    override fun execute(event: T) {
        fnWrapper.execute(event)
    }

    companion object {
        fun <T : Event> from(fnName: String, functionBinding: Value): BukkitEventListenerWrapper<T> =
            BukkitEventListenerWrapper(
                fnWrapper = PolyglotFunctionWrapper(
                    functionBinding = functionBinding,
                    name = fnName,
                    executeFn = { fn, event, _, _, _ ->
                        fn.putMember("event", event)
                        fn.execute(event)
                    }
                )
            )
    }
}
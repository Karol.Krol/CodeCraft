package kjcode.codecraft.bukkit

import kjcode.codecraft.polyglotcontext.PolyglotFunctionWrapper
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.graalvm.polyglot.Value

internal class BukkitCommandWrapper
private constructor(
    name: String,
    val fnWrapper: PolyglotFunctionWrapper<CommandSender, Command, String, Array<String>>
) : Command(name) {

    override fun execute(sender: CommandSender, commandLabel: String, args: Array<String>) =
        fnWrapper.execute(sender, this, commandLabel, args).asBoolean()

    companion object {

        fun from(name: String, functionBinding: Value): BukkitCommandWrapper = BukkitCommandWrapper(
            name = name,
            fnWrapper = PolyglotFunctionWrapper(
                functionBinding = functionBinding,
                name = name,
                executeFn = { fn, sender, cmd, label, args ->
                    fn.putMember("sender", sender)
                    fn.putMember("cmd", cmd)
                    fn.putMember("label", label)
                    fn.putMember("args", args)
                    fn.execute(sender, cmd, label, args)
                }
            )
        )
    }
}
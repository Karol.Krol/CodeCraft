package kjcode.codecraft.bukkit

import kjcode.codecraft.polyglotcontext.BindingMembersGroupingPolicy
import kjcode.codecraft.polyglotcontext.PolyglotContextBridge
import kjcode.codecraft.polyglotcontext.PolyglotContextHolder
import org.bukkit.command.CommandSender
import org.bukkit.event.Event
import org.graalvm.polyglot.Value
import org.springframework.stereotype.Component
import java.io.File

internal class JavaScriptContextHolder : PolyglotContextHolder(languageId = "js")

@Component
internal open class BukkitJavaScriptContextBridge : PolyglotContextBridge<Event, CommandSender> {

    private val nameToCommand: MutableMap<String, BukkitCommandWrapper> = HashMap()
    private val eventListenerWrappers: MutableMap<Class<Event>, BukkitEventListenerWrapper<Event>> = HashMap()

    private lateinit var javaScriptContextHolder: JavaScriptContextHolder

    override fun buildContext(file: File): Boolean = try {
        javaScriptContextHolder = JavaScriptContextHolder()
        javaScriptContextHolder.eval(file)
        registerCommands()
        registerEventListeners() // TODO: to nie jest rejestrowanie !!
        true
    } catch (ex: Exception) {
        println(ex)
        false
    }

    override fun executeCommand(sender: CommandSender, args: Array<String>): Boolean {
        if (args.isEmpty()) return false
        val commandLabel = args[0]
        val newArgs = args.copyOfRange(1, args.size)
        return nameToCommand[commandLabel]
            ?.execute(sender, commandLabel, newArgs)
            ?: false
    }

    override fun getEventListeners() = eventListenerWrappers.toList()

    override fun listCommands() = nameToCommand.keys

    override fun listHandledEvents() = eventListenerWrappers.keys.map { it.simpleName }

    override fun refreshContext(file: File) = try {
        javaScriptContextHolder.close()
        buildContext(file)
        true
    } catch (ex: Exception) {
        println(ex)
        false
    }

    override fun closeContext() = javaScriptContextHolder.close()

    private fun registerCommands() {
        nameToCommand.putAll(
            javaScriptContextHolder.groupMembersByPolicy(
                groupingPolicy = COMMANDS_GROUPING_POLICY,
                wrapperBuilder = BukkitCommandWrapper::from
            )
        )
    }

    private fun registerEventListeners() {
        eventListenerWrappers.putAll(
            javaScriptContextHolder.groupMembersByPolicy(
                groupingPolicy = EVENT_LISTENERS_GROUPING_POLICY,
                wrapperBuilder = { fnName: String, binding: Value -> BukkitEventListenerWrapper.from(fnName, binding) })
        )
    }

    private companion object {

        val COMMANDS_GROUPING_POLICY = BindingMembersGroupingPolicy(
            groupingPattern = Regex("""^on(\w*)Command$"""),
            transformKey = { it }
        )
        val EVENT_LISTENERS_GROUPING_POLICY = BindingMembersGroupingPolicy(
            groupingPattern = Regex("""^on(\w*Event)$"""),
            transformKey = BukkitEventPackagesDict::findEventClassBy
        )
    }
}
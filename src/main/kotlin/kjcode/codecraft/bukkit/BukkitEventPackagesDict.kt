package kjcode.codecraft.bukkit

import org.bukkit.event.Event

internal object BukkitEventPackagesDict {

    private const val ROOT_EVENT_PACKAGE = "org.bukkit.event"
    private val EVENT_PACKAGES = listOf(
        "block", "command", "enchantment", "entity", "hanging", "inventory", "player", "raid", "server", "vehicle",
        "weather", "world"
    ).map { "$ROOT_EVENT_PACKAGE.$it" }

    fun findEventClassBy(name: String): Class<Event>? {
        return EVENT_PACKAGES
            .map { "$it.$name" }
            .map { findClass(it) }
            .first { it != null }
    }

    private fun findClass(className: String): Class<Event>? = try {
        @Suppress("UNCHECKED_CAST")
        Class.forName(className)?.let { it as Class<Event> }
    } catch (ex: ClassNotFoundException) {
        null
    } catch (ex: ClassCastException) {
        null
    }
}
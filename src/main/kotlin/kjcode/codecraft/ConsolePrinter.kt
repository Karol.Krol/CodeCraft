package kjcode.codecraft

internal object ConsolePrinter {

    fun println(prefix: String, message: String) {
        println(
            "[${TextColor.BHI_GREEN}${prefix}${TextColor.RESET}] " +
                    "${TextColor.BHI_BLUE}${message}${TextColor.RESET}"
        )
    }

    enum class TextColor(private val value: String) {
        RESET("\u001B[0m"),
        BHI_GREEN("\u001B[1;92m"),
        BHI_BLUE("\u001B[1;94m");

        override fun toString() = value
    }
}
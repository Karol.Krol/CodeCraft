package kjcode.codecraft

import java.io.File

internal object SourceProvider {

    fun loadFile(fileName: String): File? = try {
        val dataFolder = File(SourceProvider::class.java.protectionDomain.codeSource.location.path).parentFile
        File(dataFolder, "/CodeCraft/$fileName")
    } catch (ex: Exception) {
        null
    }
}
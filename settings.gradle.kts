
rootProject.name = "CodeCraft"

pluginManagement {
    plugins {
        id("com.github.johnrengelman.shadow") version "6.1.0"
    }
}